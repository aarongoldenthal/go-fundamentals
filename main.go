package main

import (
	"bufio"
	"demo/menu"
	"fmt"
	"os"
	"strings"
)

func main() {

loop:
	for {
		fmt.Println("Please select an option:")
		fmt.Println("1) Print menu")
		fmt.Println("2) Add an item")
		fmt.Println("q) quit")
		in := bufio.NewReader(os.Stdin)
		choice, _ := in.ReadString('\n')

		switch strings.TrimSpace(choice) {
		case "1":
			menu.Print()
		case "2":
			err := menu.Add()
			if err != nil {
				fmt.Printf("\n%s\n\n", err)
			}
		case "q":
			fmt.Println("bye")
			// By default break will break out of the switch
			break loop
		default:
			fmt.Println("You chose poorly")
		}
	}
}
