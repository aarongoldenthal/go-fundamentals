// Package menu provides functionality for managing the menu
package menu

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strings"
)

type menuItem struct {
	name   string
	prices map[string]float64
}

type menu []menuItem

func (m menu) print() {
	for _, item := range m {
		fmt.Println(item.name)
		fmt.Println(strings.Repeat("-", 20))
		for size, price := range item.prices {
			fmt.Printf("%10s%10.2f\n", size, price)
		}
		fmt.Printf("\n")
	}
}

// Print writes the formatted menu to stdout
func Print() {
	data.print()
}

func (m *menu) add() error {
	fmt.Println("Please enter the name of the new item:")
	in := bufio.NewReader(os.Stdin)
	name, _ := in.ReadString('\n')
	name = strings.TrimSpace(name)

	if name == "" {
		return errors.New("item name cannot be blank")
	}

	for _, item := range data {
		if item.name == name {
			return errors.New("menu item already exists")
		}
	}

	*m = append(*m, menuItem{name: name, prices: make(map[string]float64)})
	return nil
}

// Add prompts the user for a new menu item name and adds it to the menu
func Add() error {
	return data.add()
}
